'use strict';

var ENEMY_EVERY = 60;
self.x = self.x + self.x_velo
function Game(mainEl, name, endMessage) {
  self.endMessage = endMessage;
  self.name = name;
  self = this;
  
  self.mainEl = mainEl;
  self.onEnded;
  
  self.finish = false;
  self.frame = 0;
  self.score = 0;
  self.width = window.innerWidth;
  self.height = window.innerHeight;
  self.timer = 90;
  self.health = 100;
  self.type = ['mices','dogs', 'dogs', 'cactus', 'cactus', 'cactus', 'balls', 'balls', 'balls','tuna','tuna','tuna','food', 'food','dogs','balls','balls']
  
  //create dom el

  var sound = new Audio ('./sounds/gameSound.wav')
  sound.play();
  
  self.sound = sound;

  var loseSound = new Audio ('./sounds/loseSound.wav')
  self.loseSound = loseSound;

  function getRandomTypes() {
    var arrlength = self.type.length;
    var random = Math.floor(Math.random()*arrlength)
    return self.type[random];
  
}
  
  // create timer
  function countTwoMinutes(){
    self.currentTime = Date.now();
    self.delta = self.currentTime - self.startTime;
    self.timer -= self.delta/1000;
    self.updateTime(self.delta/1000);
    self.startTime = self.currentTime;
    
  }

  self.updateTime = function (){
    if (self.timer >=90){
      self.timer = 0;
      self.clearInterval(self.loop);
     
    }
  }




  self.startTime = Date.now();
  self.timer = 90;
  self.loop = setInterval(countTwoMinutes, 1000);

  
  self.canvasEl = document.createElement('canvas');
  self.canvasEl.width = self.width;
  self.canvasEl.height = self.height;
  mainEl.appendChild(self.canvasEl);

  self.ctx = self.canvasEl.getContext('2d');
  

  // create player and enemies

  self.player = new Player(self.ctx, self.width, self.height, self.health);
  self.enemies = [];
 

  self.handleKeyDown = function(event) {
    var key = event.key.toLowerCase();
    switch(key) {
      case 'w':
      self.player.jump();
      break;
      case 'a':
      self.player.setDirection('W');
        break;
      case 'd':
      self.player.setDirection('E');
        break;
      case 's':
      self.player.ducking();
        break;
      case 'q':
      self.sound.pause()
        break;
      case 'e':
      self.sound.play()
    }
  }

  self.handleKeyUp = function (event){
    var key = event.key.toLowerCase();
    switch(key){
      case 's':
      self.player.idle('stop');
      break;
      case 'a':
      self.player.setDirection('stop1')
      break;
      case 'd':
      self.player.setDirection('stop2')
      break;
    }
  }
  document.addEventListener('keydown', self.handleKeyDown)
  document.addEventListener('keyup', self.handleKeyUp)


  
  //animation

  function doFrame() {
    
    self.frame++;
    self.player.update();
    
    

    if (self.frame % ENEMY_EVERY === 2) {
      var type = getRandomTypes();
      self.enemies.push(new Enemy(self.ctx, self.width, self.height*0.87, type));
    }
  
    self.enemies = self.enemies.filter(function (item) {

      if (item.x + item.width && !item.collided> 0 && item.y + item.height < 870 && item.y + item.height > 520){
        return true;
      } else {

        return false;
      }
    });

    self.ctx.clearRect(0, 0, self.width, self.height);

    // draw scenario

    self.ctx.beginPath(); 
    self.ctx.moveTo(0,self.height*0.10);
    self.ctx.lineTo(self.width,self.height*0.10);
    self.ctx.stroke();

    self.ctx.beginPath(); 
    self.ctx.moveTo(self.width*0.17, 0);
    self.ctx.lineTo(self.width*0.17, 76);
    self.ctx.stroke();

    self.ctx.beginPath(); 
    self.ctx.moveTo(self.width*0.59, 0);
    self.ctx.lineTo(self.width*0.59, 76);
    self.ctx.stroke();

    self.ctx.beginPath(); 
    self.ctx.moveTo(self.width*0.81, 0);
    self.ctx.lineTo(self.width*0.81, 76);
    self.ctx.stroke();




    self.ctx.fillStyle = 'black'
    self.height*0.87
    self.ctx.fillText('Score:' + self.score , 20, 50);
    

    self.ctx.fillStyle = 'black'
    self.ctx.font = '40px Monoton'
    self.ctx.fillText('Time   left:' + Math.round(self.timer), 1300, 50);
    

    self.ctx.fillStyle = 'black'
    self.ctx.fillText('Cat   name:   '+ name, 280, 50);
    self.ctx.fillStyle = 'red'
    self.ctx.fillText('Health: '+ self.health + '%', 950, 50);

    self.ctx.fillStyle = 'black'
    self.ctx.fillText('Sound off - Q / Sound on - E', 20, 130);
    


    //line
    self.ctx.beginPath(); 
    self.ctx.moveTo(0,self.height*0.98);
    self.ctx.lineTo(self.width,self.height*0.98);
    self.ctx.stroke();

    // draw the objects
    self.player.draw();
    self.enemies.forEach(function (enemy) {
      enemy.draw();
    });
    
  
    //collisions
    
    self.enemies.forEach(function (enemy) {
       
      var playerCollideRight = self.player.x + self.player.width/2 - 50 > enemy.x;
      var playerCollideTop = self.player.y  + self.player.height/2 -20> enemy.y;
      var playerCollideLeft = self.player.x - self.player.width/2 + 30  < enemy.x + enemy.width;
      var playerCollideBotttom = self.player.y - self.player.height/2 + 20 < enemy.y + enemy.height;

      if (playerCollideRight && playerCollideLeft && playerCollideTop && playerCollideBotttom) {
        self.player.collideWith(enemy);
        enemy.setCollided();
          if (enemy.type === 'dogs') {
            self.health = self.health -50
          } else if (enemy.type === 'balls'){
            self.health = self.health -20
          }else if (enemy.type === 'mices'){
            self.score = self.score +50
          }else if (enemy.type === 'tuna'){
            self.score = self.score +30
          }else if (enemy.type === 'cactus'){
            self.health = self.health -10
          }else if (enemy.type === 'food'){
            self.score = self.score +20
          }else {
            self.score = self.score +100
          }
        }
        // @todo update score here, depending on enemy type, whatever
      
      
    });
  
    if (self.timer <= 0){
      self.endMessage = 'only ' +self.score + ' '+'points ?? Try again, you can do better !!';
      self.onEnded();
      self.sound.pause();
      self.loseSound.play();
    } else if (self.health <= 0){
      self.endMessage = 'you killed your cat. Score ' +self.score + ' .Try again, cats have 9 lives!!';
      self.onEnded();
      self.updateTime;
      self.sound.pause();
      self.loseSound.play();
    } 

    if (!self.finish) {
      window.requestAnimationFrame(doFrame);
    
    }
  }
  window.requestAnimationFrame(doFrame);
}



Game.prototype.destroy = function () {
  var self = this;
  self.finish = true;
  self.canvasEl.remove();
  document.removeEventListener('keydown',self.handleKeyDown);
};

Game.prototype.onGameOver = function (callback) {
  var self = this;
  self.onEnded = callback;
 
};

