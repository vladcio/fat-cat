'use strict';

function main() {

  var mainEl = document.querySelector('#site-main');
  var name;
  var endMessage;

  var HallOfFame = [];
 
  var stage;
  var game;

  var instrEl
  // splash
  var splashEl;
  var startButton;
  var instructionsButton;
  
  var handleStartClick = function () {
    var name = document.getElementById('name').value;
    destroySplash();
    buildGame(name, endMessage);
  };


  var handleInstrClick = function (){
    destroySplash();
    buildInstructions();
  }

  var goBackButton

  var handleGoBackClick = function (){
    destroyInstructions();
    buildSplash();
  };  

  function destroyInstructions(){
    // goBackButton.removeEventListener('click', handleGoBackClick);
    instrEl.remove();
  }

  function buildInstructions(){
    stage = 'instructions';

    instrEl = document.createElement('div');
    instrEl.setAttribute('id', 'instructions');
        
    var caracterInst = document.createElement('carInst');
    instrEl.appendChild(caracterInst);

    var title = document.createElement('h1');
    title.setAttribute('id', 'instrTitle');
    title.innerText = 'INSTRUCTIONS:';
    instrEl.appendChild(title);

    var text = document.createElement('span');
    text.setAttribute('id', 'textInstr');
    text.innerText = 'The player need to avoid all the unfriendly obstacles and collect all the food in order to get a bigger score.'
    instrEl.appendChild(text);

    var wasd = document.createElement('div');
    wasd.setAttribute('id', 'wasdImg');
    instrEl.appendChild(wasd);

    var text2 = document.createElement('span');
    text2.setAttribute('id', 'textInstr2')
    text2.innerText = 'Use key a = left, d = right, s = slide, w = jump'
    instrEl.appendChild(text2);

    var enemiesList = document.createElement('ul');
    enemiesList.setAttribute('id', 'enemiesList') ;
    enemiesList.innerText = 'Enemies to avoid:' ;
    instrEl.appendChild(enemiesList);

    var dog = document.createElement('li');
    dog.setAttribute('id', 'dog') ;
    dog.innerText = 'Dog = - 50 Health';
    enemiesList.appendChild(dog);

    var ball = document.createElement('li');
    ball.setAttribute('id', 'ball') ;
    ball.innerText = 'Ball = - 20 Health';
    enemiesList.appendChild(ball);

    var cactus = document.createElement('li');
    cactus.setAttribute('id', 'cactus') ;
    cactus.innerText = 'Cactus = - 10 Health';
    enemiesList.appendChild(cactus);

    var foodList = document.createElement('ul');
    foodList.setAttribute('id', 'foodList') ;
    foodList.innerText = 'Items: ';
    instrEl.appendChild(foodList);

    var mouse = document.createElement('li');
    mouse.setAttribute('id', 'mouse') ;
    mouse.innerText = 'Mouse = + 50 Points';
    foodList.appendChild(mouse);

    var tuna = document.createElement('li');
    tuna.setAttribute('id', 'tuna') ;
    tuna.innerText = 'Tuna = + 30 Points';
    foodList.appendChild(tuna);

    var food = document.createElement('li');
    food.setAttribute('id', 'food') ;
    food.innerText = 'Food = + 20 Points';
    foodList.appendChild(food);


    var goBackButton = document.createElement('button');
    goBackButton.setAttribute('id', 'goBackButton');
    goBackButton.innerText = 'Go Back';
    instrEl.appendChild(goBackButton);

    goBackButton.addEventListener('click', handleGoBackClick);

    mainEl.appendChild(instrEl);

  }


  function buildSplash(){
    stage = 'splash';
    HallOfFame
    // create dom el
    
    splashEl = document.createElement('div');
    splashEl.setAttribute('id', 'splash');

    var caracter = document.createElement('img');
    splashEl.appendChild(caracter);
      
    var title = document.createElement('h1');
    title.innerText = 'FAT CAT';
    splashEl.appendChild(title);

    var yourName = document.createElement('input');
    yourName.setAttribute('id', 'name');
    yourName.placeholder = 'Insert Cat Name';
    splashEl.appendChild(yourName);

    var startButton = document.createElement('button');
    startButton.setAttribute('id', 'startButton');
    startButton.innerText = 'START GAME';
    splashEl.appendChild(startButton);

    var bestScore = document.createElement('h4');
    if (HallOfFame.length >0){
    bestScore.innerText = 'Best Score: ' + HallOfFame[0].name + ' ' + HallOfFame[0].score;
    }
    splashEl.appendChild(bestScore);

    var instructionsButton = document.createElement('button');
    instructionsButton.setAttribute('id', 'instrButton');
    instructionsButton.innerText = 'Instructions';
    splashEl.appendChild(instructionsButton);

    var copyRight = document.createElement('span');
    copyRight.setAttribute('id', 'copy');
    copyRight.innerText = '© Vlad Ciobanu - 2018 - IronHack';
    splashEl.appendChild(copyRight);


    // apppend to site-main
    mainEl.appendChild(splashEl);

    // bind click on start game button
    startButton.addEventListener('click', handleStartClick);
    instructionsButton.addEventListener('click', handleInstrClick);
  }

  function destroySplash() {
    // self.startButton.removeEventListener('click', handleStartClick);
    // self.instructionsButton.removeEventListener('click', handleInstrClick);
    splashEl.remove();
  }

  // game

  function buildGame(name, endMessage) {
    var name;
    stage = 'game';
    game = new Game(mainEl, name, endMessage);

    game.onGameOver(function () {
      destroyGame();
      buildGameOver(name, endMessage);
    });
  }

  function destroyGame(){
    game.destroy();
    
    
  }

  //game over

  var gameOverEl;
  var playAgainButton;

  var handlePlayAgainClick = function (){
    destroyGameOver();
    buildSplash();
  };  


  function buildGameOver(name, endMessage) {
    stage = 'gameOver'
    var name;
    var endMessage;
    game.score

    //create dom el

    gameOverEl = document.createElement('div');
    gameOverEl.setAttribute('id', 'game-over');

    HallOfFame.push({name: name, score: game.score})
    if (game.score > HallOfFame[0].score){
      HallOfFame.shift({name: name, score: game.score})
    } 
    var bestScores = document.createElement('div')
    bestScores.setAttribute('id', 'scores')
    bestScores.innerText = 'Best Score: ' + HallOfFame[0].name + ' ' + HallOfFame[0].score;
    gameOverEl.appendChild(bestScores);

    var caracterDie = document.createElement('img2')
    caracterDie.setAttribute('id', 'img2');
    gameOverEl.appendChild(caracterDie);

    var title = document.createElement('div');
    title.setAttribute('id', 'over');
    title.innerText = 'Game Over';
    gameOverEl.appendChild(title);

    var score = document.createElement('div');
    score.setAttribute('id', 'message');
    score.innerText  = name +' '+ game.endMessage;
    gameOverEl.appendChild(score);

    playAgainButton = document.createElement('button');
    playAgainButton.setAttribute('id', 'playAgainButton');
    playAgainButton.innerText = 'Play again!';
    gameOverEl.appendChild(playAgainButton);
    

        
    // HallOfFame.sort()
    // for (var ix = 0; ix < HallOfFame.length; ix++){
    // print position(ix+1)
    // }

    //append to main

    mainEl.appendChild(gameOverEl);

    // bind play again button

    playAgainButton.addEventListener('click', handlePlayAgainClick);
  }

  function destroyGameOver(){
    playAgainButton.removeEventListener('click', handlePlayAgainClick);
    gameOverEl.remove();
  }
  buildSplash();
}


window.onload = main;
