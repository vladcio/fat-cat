'use strict';

function Enemy(ctx, width, height, type){
  var self = this;

  self.collided = false;
  self.width = 80;
  self.height = 80;
  self.ctx = ctx;
  self.type = type;
  
  self.gameWidth = width;
  self.gameHeight = height;
  self.x_velocity = -2;
  
  self.x = self.gameWidth + 1;
  self.y = self.gameHeight * Math.random();

} 

Enemy.prototype.setCollided = function () {
  var self = this;
  
  self.collided = true;

}


Enemy.prototype.draw = function() {
  var self = this;
  var picture = new Image;

  // update the state
  self.x = self.x + self.x_velocity;
  
  // draw
  if (self.type === 'dogs') {
    self.ctx.fillStyle = 'red';
    self.x_velocity = -4;
    self.width = 70;
    self.height = 150;
    picture.src = 'images/enemies/dog.gif'
  }else if (self.type === 'balls'){
    picture.src = 'images/enemies/ball.png'
    self.x_velocity = -5;
    self.width= 100;
    self.height = 100;
  }else if (self.type === 'mices'){
    picture.src = './images/enemies/mouse.gif';
    self.width = 60;
    self.height = 40;
  }else if (self.type === 'tuna'){ 
    picture.src = './images/enemies/tuna.png'
    self.width = 90;
    self.height = 70;
  }else if (self.type === 'cactus'){
    picture.src = './images/enemies/cactus.png'
    self.width =50
    self.height =80
  }else if (self.type === 'food'){
    picture.src = './images/enemies/food.png'
    self.width = 90;
    self.height = 50;
  }else {
    self.ctx.fillStyle = 'purple';
  }


  self.ctx.drawImage(picture, self.x, self.y , self.width, self.height)
}