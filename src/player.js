'use strict';

var JUMP_VELOCITY = -6;
var FALLING_VELOCITY = 0.4;

function Player(ctx, width, height, health){
  var self = this;
  
  self.width = 100;
  self.height = 100;
  self.ctx = ctx;
  self.health = health;
  self.gameWidth = width;
  self.gameHeight = height;
  
  self.dieImage = 1;
  self.x = width / 2;
  self.y = 0 ;
  self.y_velocity = FALLING_VELOCITY;
  self.x_velocity = 0;
  self.falling = true;
  self.jumping = false;
  self.jumpingImage = 1;
  self.direction = null;
  self.move = false;
  self.moveLeft = false;
  self.directionRight = 1; 
  self.directionLeft = 1;
  self.duck = false;
  self.initial = true;
  self.crouch = null;
}
  
Player.prototype.jump = function (direction) {
  var self = this;
  if (self.jumping && !self.falling) {
    return;
  } else if (!self.jumping && self.falling){
    return;
  }
  self.y_velocity = JUMP_VELOCITY;
  self.jumping = true;

}

Player.prototype.ducking = function (height, width){
  var self = this;
  if (self.duck){
    return
  } else{
  self.duck = true;
  }
}

Player.prototype.idle = function (height, width, crouch){
  var self = this;
  if (self.initial){
    return self.duck = false;
  }
}

Player.prototype.collideWith = function (theOtherOne) {
  // @todo later you can make the player do something (animation wise)
}

Player.prototype.setDirection = function (direction) {
  var self = this;

  // @todo not allow to change to opposite direction
  self.direction = direction;
}

Player.prototype.update = function() {
  var self = this;




  if (self.direction === 'E') {
    self.x += 10;
      self.move = true;
      self.initial = false;
      self.duck = false;
      self.directionRight = self.directionRight + 1
      if (self.directionRight > 8) {
        self.directionRight = 1;
      }
  }else if (self.direction === 'W') {
    self.x -= 5;
      self.moveLeft = true;
      self.initial = false;
      self.duck = false;
      self.directionLeft = self.directionLeft + 1
      if (self.directionLeft > 8) {
        self.directionLeft = 1;
      }
  }else if (self.direction === 'stop1'){
    self.moveLeft = false;
    self.initial = true;
  }else if (self.direction === 'stop2'){
    self.move = false;
    self.initial = true;
  }else if (self.idle === 'stop'){
    self.duck = false;
    self.initial = true;
  }
    
  if (self.x > self.gameWidth) {
    self.x = 0;
  }
  if (self.x < 0) {
    self.x = self.gameWidth;
  }
}



Player.prototype.draw = function () {
  var self = this;
  var picture = new Image();
  self.duck;
  self.jump;
  self.initial;
  self.health
  // update the state



  if (self.falling) {
    self.y_velocity /= 0.92;
    self.jumpingImage = self.jumpingImage - 0.12;
    if (self.jumpingImage < 1) {
      self.jumpingImage = 1;
    }
  }

  if (self.jumping) {
    self.y_velocity *= 0.99;
    self.jumpingImage = self.jumpingImage + 0.08;
    if (self.jumpingImage > 8) {
      self.jumpingImage = 8;
    }
  }

  self.y += self.y_velocity;

  // stop jumping?
  if (self.jumping && self.y_velocity > -3) {
    self.y_velocity = FALLING_VELOCITY;
    self.jumping = false;
    self.falling = true;
  }

  // stop falling?
  if (self.falling && self.y > self.gameHeight*0.98 - self.height / 2) {
    self.y = self.gameHeight*0.98 - self.height / 2;
    self.y_velocity = 0;
    self.falling = 0;
  }
 
  
  // draw
  if (self.duck === true ){
    picture.src = 'images/player/slide/1.png'
  } else if (self.jumping === true || self.falling === true){
    picture.src = 'images/player/jump/' + Math.floor(self.jumpingImage) + '.png'
  }else if (self.initial === true){
    picture.src = './images/player/idle/1.png'
  }else if (self.move === true) {
    picture.src = './images/player/run/'+ Math.floor(self.directionRight) + '.png'
  }else if (self.moveLeft === true) {
    picture.src = './images/player/runLeft/'+ Math.floor(self.directionLeft) + '.png'
  }else if (self.health <= 0){
    picture.src = './images/player/dead/9.png'
    }else{
    picture.src = './images/player/idle/1.png'
  }
  self.ctx.fillStyle = 'blue';
  self.ctx.drawImage(picture, self.x - self.width/2, self.y - self.height/2, self.width, self.height)
}

